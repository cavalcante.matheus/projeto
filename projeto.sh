#!/bin/bash


obter_entrada_usuario() {
    yad --title "Informe o usuário e o host remoto" \
        --form \
        --field "Usuário@Host Remoto" \
        --button="Enviar":0 \
        --button="Cancelar":1
}

selecionar_arquivo() {
    yad --title "Selecione um arquivo" \
        --file \
        --button="Selecionar":0 \
        --button="Cancelar":1
}


exibir_dialogo_informacoes() {
    yad --title "Informação" \
        --info \
        --text "$1" \
        --button="OK":0
}


exibir_dialogo_erro() {
    yad --title "Erro" \
        --error \
        --text "$1" \
        --button="OK":0
}


exibir_dialogo_confirmacao() {
    yad --title "Confirmação" \
        --text "$1" \
        --button="Sim":0 \
        --button="Não":1
}


copiar_do_remoto() {
    arquivo_remoto=$1
    arquivo_local=$2

    scp "$usuario_host:$arquivo_remoto" "$arquivo_local"
    if [ $? -eq 0 ]; then
        exibir_dialogo_informacoes "Arquivo copiado com sucesso para o host local!"
    else
        exibir_dialogo_erro "Falha ao copiar o arquivo do host remoto."
    fi
}


copiar_para_remoto() {
    arquivo_local=$1
    arquivo_remoto=$2

    scp "$arquivo_local" "$usuario_host:$arquivo_remoto"
    if [ $? -eq 0 ]; then
        exibir_dialogo_informacoes "Arquivo copiado com sucesso para o host remoto!"
    else
        exibir_dialogo_erro "Falha ao copiar o arquivo para o host remoto."
    fi
}


listar_arquivos_remotos() {
    arquivos=$(ssh "$usuario_host" ls)
    if [ $? -eq 0 ]; then
        exibir_dialogo_informacoes "Arquivos no host remoto:\n\n$arquivos"
    else
        exibir_dialogo_erro "Falha ao listar os arquivos no host remoto."
    fi
}


salvar_host() {
    echo "$usuario_host" > host_salvo.txt
    exibir_dialogo_informacoes "Host remoto salvo com sucesso!"
}


verificar_instalacao_ssh() {
    ssh "$usuario_host" -q exit
    if [ $? -eq 0 ]; then
        exibir_dialogo_informacoes "O servidor SSH está instalado no host remoto."
    else
        exibir_dialogo_erro "O servidor SSH não está instalado no host remoto."
    fi
}


if [ -f host_salvo.txt ]; then
    usuario_host=$(cat host_salvo.txt)
fi


usuario_host=$(obter_entrada_usuario)
botao=$?


if [ $botao -eq 0 ]; then
    escolha=$(yad --title "Menu" \
                  --text "Escolha uma opção:" \
                  --button="Copiar do Host Remoto para Local":0 \
                  --button="Copiar do Local para o Host Remoto":1 \
                  --button="Visualizar arquivos do Host Remoto":2 \
                  --button="Salvar Host Remoto":3 \
                  --button="Verificar instalação do SSH no Host Remoto":4 \
                  --button="Sair":5)
    botao=$?
    case $botao in
        0)  
            arquivo_remoto=$(selecionar_arquivo)
            if [ $? -eq 0 ]; then
                copiar_do_remoto "$arquivo_remoto" .
            fi
            ;;
        1)  
            arquivo_local=$(selecionar_arquivo)
            if [ $? -eq 0 ]; then
                copiar_para_remoto "$arquivo_local" .
            fi
            ;;
        2)  
            listar_arquivos_remotos
            ;;
        3)  
            salvar_host
            ;;
        4)  
            verificar_instalacao_ssh
            ;;
         *) 
            exit 0
            ;;
    esac
fi
